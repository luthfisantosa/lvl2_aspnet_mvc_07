﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LVL2_ASPnet_MVC_07.Controllers
{
    public class PersonController : Controller
    {
        // GET: Person
        [HttpGet]
        public ActionResult Index()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1000);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/PersonReport";

            ViewBag.ReportViewer = report;
            return View();
        }

        [HttpPost]
        public ActionResult Index(string firstName, string midName, string lastName)
        {
            ReportParameter p = new ReportParameter("firstName", firstName);
            ReportParameter p2 = new ReportParameter("midName", midName);
            ReportParameter p3 = new ReportParameter("lastName", lastName);
            

            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1000);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/PersonReport";

            report.ServerReport.SetParameters(new ReportParameter[] { p, p2, p3 });

            ViewBag.ReportViewer = report;
            return View();
        }
    }
}